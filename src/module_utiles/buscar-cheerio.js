const cheerio = require('cheerio');
const request = require('request-promise');

async function accederPagina(page){
    let date = new Date()
    //trato que cada cabezera sea distinta en las peticiones.(intuicion)
    //console.log(date.toLocaleTimeString());
    return await request({
        uri: page,
        headers: {
            'User-Agent': 'Request-Promise-'+date.toLocaleTimeString()
        },
        timeout:10000,
        transform: body => cheerio.load(body)
    });
}

async function buscarPagUtiles($page){

    const url = 'https://es.investing.com';

    let cuentaResultados='';
    let ratios='';
    let pages = $page('li[data-test="Fundamental"] a');
    
    for (let i = 0; i < pages.length; i++){
        const category = $page(pages[i]).attr('data-test');

        if(category=='Cuenta-de-resultados'){
            cuentaResultados= $page(pages[i]).attr('href');
        }
        if(category=='Ratios'){
            ratios= $page(pages[i]).attr('href');
        }
    }
    //valido que no esten
    if(!cuentaResultados || !ratios){
        return false
    }
    return{
        cuentaResultados: url+cuentaResultados,
        ratios: url+ratios
    }
}

async function buscarIngresosTotales($page){
    let ingresosTotales = [];
    let $ingresos = $page('tr#parentTr.openTr.pointer td');
    let $dates = $page('#header_row.alignBottom th');

    for (let i=0; i<4; i++){
        $ingresos = $ingresos.next();
        $dates = $dates.next();

        value = parseFloat($ingresos.html().replaceAll('.','').replace(',','.'))
        
        if(isNaN(value)|| !value || !$dates.children().html()){
            return false;
        }
        ingresosTotales.push({
            date:$dates.children().html()+'-'+$dates.children().next().html(),
            value: value
        });
    }
    return ingresosTotales;   
}

async function buscarRatios($page, mensaje){
    let res = []; 
    $page('td.innerTD tr').each((i,el)=>{
        let aux = $page(el).text().split('\n').slice(1,4); //
        if(aux[0]== mensaje){
            res=[
                parseFloat(aux[1].replaceAll('.','').replace(',','.')),
                parseFloat(aux[2].replaceAll('.','').replace(',','.'))
            ];
            return false; // para dear de iterar.
        }
    })
    //valido resultados
    //creo que falta validar por si es vacio el array.
    if(!res[0] || !res[1]){
        return false;
    }
    return res;
}

async function buscarRentabilidad($page){
    return await buscarRatios($page,'Rentabilidad sobre fondos propios TTM');
}

async function buscarPrecioVenta($page){
    return await buscarRatios($page,'Precio/Ventas TTM');
}

module.exports={
    accederPagina:accederPagina,
    buscarPagUtiles:buscarPagUtiles,
    buscarIngresosTotales:buscarIngresosTotales,
    buscarRentabilidad:buscarRentabilidad,
    buscarPrecioVenta:buscarPrecioVenta,
}