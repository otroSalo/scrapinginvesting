const fs = require('fs');
const empresas = JSON.parse(fs.readFileSync('./empresas.json',"UTF-8"));

const {accederPagina,buscarPagUtiles, buscarIngresosTotales, buscarPrecioVenta, buscarRentabilidad} = require('./module_utiles/buscar-cheerio');

const {appendLog, appendInform, initLogEmpresa, appendEjectInfo} = require('./generarInformes/informes');

const {analizarComprar, analizarVender} = require('./module_utiles/funciones-analisis');

const {folderId} = require('./generarInformes/userDrive');

(async()=>{

    //inicio contador total
    let sumaTime = 0;
    let contador = 0;

    let init = await initLogEmpresa(folderId? true:false); // true si quiero usar la nuve de drive.
    let numInit = init.numIni; // por que empresa inicia del directorio;
    let sheets = init.sheets; // sheets abiertos
    let googleId = init.googleId; // id del sheet

    for(let posEmpresa =numInit; posEmpresa < empresas.length; posEmpresa++){
        
        /*Da inicio al proceso por empresa */
        const start = new Date();

        let resEmpresa = {
            name : empresas[posEmpresa].name,
            ref : empresas[posEmpresa].ref,
            ingresosTotales : [],
            rentabilidad : [],
            precioVenta: [],
            recomendacion: '-',
            href: empresas[posEmpresa].src
        };

        try{
            //pagina principal        
            let $ = await accederPagina(empresas[posEmpresa].src);
        
            //paginas utiles
            let pages = await buscarPagUtiles($);

            //pagina Cuenta resultados
            let $cuentaResultados = await accederPagina(pages.cuentaResultados)
            //ingresos totales
            resEmpresa.ingresosTotales = await buscarIngresosTotales($cuentaResultados);
            if(!resEmpresa.ingresosTotales)
                throw {name:"Error", message:"Datos invalido en ingresos totales."};
            //pagina Ratios
            let $ratios = await accederPagina(pages.ratios);
            //rentablilidad
            resEmpresa.rentabilidad = await buscarRentabilidad($ratios);
            if(!resEmpresa.rentabilidad)
                throw {name:"Error", message:"Datos invalidos en ratio rentabilidad." };

            resEmpresa.precioVenta = await buscarPrecioVenta($ratios);
            if(!resEmpresa.precioVenta)
                throw new {name:"Error", message:"Datos invalidos en ratio precio venta"};

            /*EMPIEZO A ANALIZAR LOS RESUTLADOS */

            /*Aca filtramos analisis para comprar */
            if(analizarComprar(resEmpresa))
                resEmpresa.recomendacion='Comprar';
             
            /*Segundo es recomendable vender??? */
            if(analizarVender(resEmpresa))
                resEmpresa.recomendacion='Vender';

            if(!await appendInform(resEmpresa,sheets))
                throw {name:"Error", message:"Falló escritura de informe en drive"};

            throw {name:"OK", message:"Empresa analizada correctamente" }
            
        }catch(e){
            //console.error(empresas[posEmpresa],e);
            //cantidad de veces que intenta refrecar la pagina si tiene algun error.
            if(e.name == 'RequestError'){
                if(contador<2){
                    posEmpresa --;
                    contador ++;
                }else{
                    appendLog(resEmpresa,e.name+" : "+"Error al cargar la pagina");
                    contador = 0;
                }
            }else{
                //console.log(e.name+" : "+e.message);
                appendLog(resEmpresa,e.name+" : "+e.message);
            }
        }
        //Finaliza el proceso por empresa
        const end = new Date();
        sumaTime = sumaTime + (end-start)/1000;
        console.log(`el tiempo de ejecucion para la empresa ${resEmpresa.name} es : ${(end-start)/1000}`);
    }
    //Damos por finalizado todo el proceso.
    appendEjectInfo(new Date(), 'Termino de analizarse las empresas del directorio',googleId);

})();
