
const fs = require('fs');
const v = require('./variables');

/*--------------------Google Drive-------------------------------- */
const {google}=require('googleapis');

//const credenciales = require('./credenciales.json');
const {credenciales, folderId}= require('./userDrive');

const scopes = ['https://www.googleapis.com/auth/drive'];
const auth = new google.auth.JWT(credenciales.client_email,null,credenciales.private_key, scopes);
const drive = google.drive({version:'v3', auth});
const sheetsGoogle = google.sheets({version:'v4', auth});

/*-----------------------Google Drive Sheet-------------------------*/
const {GoogleSpreadsheet} = require('google-spreadsheet');

//const googleId = '1ZIk3xz3wjQ5YNpbj0zSqyyuDBt5jwBZUXPr3b1Pn3e0';
//const folderId = '1bCugoHUfRtXxWDRD2CWMvilAn7Jvy2WH';

const iniInfCompleto = (resEmpresa) =>{     
    return  { 
        Nombre:resEmpresa.name,
        Ref:resEmpresa.ref,
        Fecha1IngresoTotal:resEmpresa.ingresosTotales[0]?.date, Monto1IngresoTotal:resEmpresa.ingresosTotales[0]?.value,
        Fecha2IngresoTotal:resEmpresa.ingresosTotales[1]?.date, Monto2IngresoTotal:resEmpresa.ingresosTotales[1]?.value,
        Fecha3IngresoTotal:resEmpresa.ingresosTotales[2]?.date, Monto3IngresoTotal:resEmpresa.ingresosTotales[2]?.value,
        Fecha4IngresoTotal:resEmpresa.ingresosTotales[3]?.date, Monto4IngresoTotal:resEmpresa.ingresosTotales[3]?.value,
        EmpresaRentabilidadTTM:resEmpresa.rentabilidad[0], IndustriaRentabilidadTTM:resEmpresa.rentabilidad[1],
        EmpresaPrecioVentaTTM:resEmpresa.precioVenta[0], IndustriaPrecioVentaTTM:resEmpresa.precioVenta[1],
        Recomendación:resEmpresa.recomendacion,
        URL:resEmpresa.href
    }
}

const iniInfResumido = (resEmpresa) =>{
    return {
        Nombre:resEmpresa.name,
        Ref:resEmpresa.ref,
        Recomendación:resEmpresa.recomendacion,
        URL:resEmpresa.href
    }
}

async function createSheet(){
    /*CREAR UNA NUEVA HOJA DE CALCULO*/
    let newSheet = await sheetsGoogle.spreadsheets.create({
        resource:{
            properties:{title:v.informe.informeDrive},
            sheets:[
                {properties:{title:v.informe.sheetCompleto}},
                {properties:{title:v.informe.sheetResumen}}
            ]
        },
    });
    /*MOVER LA HOJA DE CALCULO*/
    await drive.files.update({
        fileId: newSheet.data.spreadsheetId,
        addParents: folderId,
        fields: 'id, parents'
    });

    /*INICIALIZAR SHEETS*/
    await sheetsGoogle.spreadsheets.values.append({
        spreadsheetId: newSheet.data.spreadsheetId,
        valueInputOption: 'USER_ENTERED',
        range: `${v.informe.sheetCompleto}!A1:A1`,
        resource: {
            range: `${v.informe.sheetCompleto}!A1:A1`,
            majorDimension: 'ROWS',
            values: v.informe.encabezadoSheetCompleto,
        },
    });

    await sheetsGoogle.spreadsheets.values.append({
        spreadsheetId: newSheet.data.spreadsheetId,
        valueInputOption: 'USER_ENTERED',
        range: `${v.informe.sheetResumen}!A1:A1`,
        resource: {
            range: `${v.informe.sheetResumen}!A1:A1`,
            majorDimension: 'ROWS',
            values: v.informe.encabezadoSheetResumen,
        },
        
    });

    return newSheet?.data?.spreadsheetId;
}


/*--------------Informe local encabezados--------- */
async function searchEndGoogleId(){
    let cont = fs.readFileSync('./outFiles/historyEject.csv','utf-8');
    let arrayCont =cont.split('\n');
    let arrayRow = arrayCont[arrayCont.length-2].split(';');
    //falta validar que no haya espacios en blacno por que el formato del informe no me lo rompa.

    //falta validar que haya encontrado lo que estaba buscando.
    return arrayRow[arrayRow.length-1].replace(' ','');
}


async function initLogEmpresa(isUsedDrive){

    let sheets = ['',''];
    let googleId = '';

    if(!fs.existsSync(v.informe.logsArchivo)){        
        if(isUsedDrive){
            //Creamos Infrome en la nuve de drive
            googleId = await createSheet();
            //accedemos a los sheets
            sheets = await accederGoogleSheet(googleId);
        }
        //Si no existe se crea el archivo
        fs.writeFileSync(v.informe.logsArchivo, v.informe.logsEncabezado);
        appendEjectInfo(new Date(),'Inicia el analisis de las empresas del directorio',googleId);

        return {numIni:0, sheets:sheets, googleId:googleId};
    }

    //Si existe y solo quiero retomar donde lo deje
    if(isUsedDrive){
        googleId = await searchEndGoogleId();
        sheets = await accederGoogleSheet(googleId);
    }

    //Nos fijamos en que empresa nos quedamos
    let cont = fs.readFileSync(v.informe.logsArchivo,'utf-8');
    return {numIni:cont.split('\n').slice(6).length-1, sheets:sheets, googleId:googleId};
};


async function accederGoogleSheet(googleId){
    const document = new GoogleSpreadsheet(googleId);
    await document.useServiceAccountAuth(credenciales);
    await document.loadInfo();
    const sheets = document.sheetsByIndex;
    return sheets;
}

async function appendReducido(resEmpresa,sheetReducido){
    try{
        //Informe drive
        let info = iniInfResumido(resEmpresa);
        await sheetReducido.addRow(info);
        return true;

    }catch(e){
        //Archivo Local
        if(!fs.existsSync(v.informe.resumidoArchivo))
            fs.writeFileSync(v.informe.resumidoArchivo, v.informe.resumidoEncabezado);
            
        let contenido = `${resEmpresa.name};${resEmpresa.ref};${resEmpresa.recomendacion};${resEmpresa.href}\n`
        fs.appendFileSync(v.informe.resumidoArchivo,contenido);
        
        return false;
    }
}

async function appendCompleto(resEmpresa,sheetCompleto){
    try{
        //--drive--
        let info = iniInfCompleto(resEmpresa);
        await sheetCompleto.addRow(info);
        return true;
    }catch(e){
        //Archivo local
        if(!fs.existsSync(v.informe.completoArchivo))
            fs.writeFileSync(v.informe.completoArchivo, v.informe.completoEncabezado);

        let contenido = `${resEmpresa.name};${resEmpresa.ref};${resEmpresa.ingresosTotales[0]?.date};${resEmpresa.ingresosTotales[0]?.value};${resEmpresa.ingresosTotales[1]?.date};${resEmpresa.ingresosTotales[1]?.value};${resEmpresa.ingresosTotales[2]?.date};${resEmpresa.ingresosTotales[2]?.value};${resEmpresa.ingresosTotales[3]?.date};${resEmpresa.ingresosTotales[3]?.value};${resEmpresa.rentabilidad[0]};${resEmpresa.rentabilidad[1]};${resEmpresa.precioVenta[0]};${resEmpresa.precioVenta[1]};${resEmpresa.recomendacion};${resEmpresa.href}\n`;
        fs.appendFileSync(v.informe.completoArchivo, contenido);

        return false;  
    }
}

async function appendInform(resEmpresa,sheets){
    
    //sheets[0]-> completo; sheets[1]->reducido
    if(!resEmpresa || !sheets.length)
        return false;
    
    let res=true;

    if(resEmpresa.recomendacion=="-"){
        if(!await appendCompleto(resEmpresa,sheets[0]))
            res = false;
    }
    else{
        if(!await appendCompleto(resEmpresa,sheets[0]))
            res = false;

        if(!await appendReducido(resEmpresa,sheets[1]))
            res = false;
    }
    return res;
}

async function appendLog(resEmpresa,mensaje){
    let contenido = `${resEmpresa.name} ;${resEmpresa.ref} ;${resEmpresa.href} ;${mensaje}\n`;
    fs.appendFileSync(v.informe.logsArchivo,contenido);
}

async function appendEjectInfo(date,mensaje,googleId){
    let contenido = `${date}; ${mensaje}; ${googleId}\n`;
    fs.appendFileSync('./outFiles/historyEject.csv', contenido)
}

module.exports={
    accederGoogleSheet:accederGoogleSheet,
    appendReducido:appendReducido,
    appendCompleto:appendCompleto,
    appendLog:appendLog,
    appendInform:appendInform,
    initLogEmpresa:initLogEmpresa,
    appendEjectInfo:appendEjectInfo,
}

