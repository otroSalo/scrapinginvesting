const fs = require('fs');

const version = () =>{
    if(fs.existsSync('./outFiles/historyEject.csv')){
        let cont = fs.readFileSync('./outFiles/historyEject.csv','utf-8')
        if(cont){
            let aux =cont.split('\n').length-2;//-la cabezara y el ultimo que agrego
            if((aux)%2==0){
                return 'v'+((aux/2)+1);
            }
            return 'v'+(((aux-1)/2)+1);
        }else{
            return 'v'+1;
        }
    }else{
        let encabezado = 'Fecha; Mensaje; IdGoogleSheets\n'
        fs.writeFileSync('./outFiles/historyEject.csv',encabezado);
        return 'v'+1;
    }
}
const versionText = version();
const informeCompleto = `./outFiles/informeCompleto-${versionText}.csv`;
const informeResumido = `./outFiles/informeResumido-${versionText}.csv`;
const informeLogs = `./outFiles/informeLogs-${versionText}.csv`;

/*-------formato Drive -------*/
const informeDrive=`InformeInvesting-${versionText}`;
const sheetCompleto='Completo';
const sheetResumen='Resumen';
const encabezadoSheetCompleto = [['Nombre','Ref','Fecha1IngresoTotal','Monto1IngresoTotal','Fecha2IngresoTotal','Monto2IngresoTotal','Fecha3IngresoTotal','Monto3IngresoTotal','Fecha4IngresoTotal','Monto4IngresoTotal','EmpresaRentabilidadTTM','IndustriaRentabilidadTTM','EmpresaPrecioVentaTTM','IndustriaPrecioVentaTTM','Recomendación','URL']];
const encabezadoSheetResumen=[['Nombre','Ref','Recomendación','URL']];


const fecha = new Date();


let rotuloInfCompleto = `Informe Completo;;;;;;;;;;;;;;;\nTipo : Acciones;;;;;;;;;;;;;;;\nPais: EEUU;;;;;;;;;;;;;;;\nFecha de creación : ${fecha.getDate()}/${fecha.getMonth()+1}/${fecha.getFullYear()};;;;;;;;;;;;;;;\n;;;;;;;;;;;;;;;\n`
let encabezadoInfCompleto = `${rotuloInfCompleto}Nombre;Ref.;Ingresos Totales;;;;;;;;Rentabilidad sobre fondos propios TTM;;Precio/Venta TTM;;Recomendación;URL\n;;fecha;monto;fecha;monto;fecha;monto;fecha;monto;Empresa;Industria;Empresa;Industria;;\n`;

let rotuloInfResumido =`Informe Resumido;;;\nTipo : Acciones;;;\nPais: EEUU;;;\nFecha de creación : ${fecha.getDate()}/${fecha.getMonth()+1}/${fecha.getFullYear()};;;\n;;;\n`
let encabezadoInfResumido = `${rotuloInfResumido}Nombre;Ref.;Recomendación;URL\n`;

let rotuloInfLogs = `Informe de Errores;;;;\nTipo : Acciones;;;;\nPais : EEUU;;;;\nFecha de creación: ${fecha.getDate()}/${fecha.getMonth()+1}/${fecha.getFullYear()};;;;\n;;;;\n`
let encabezadoInfLogs = `${rotuloInfLogs}Nombre;Ref.;URL;Mensaje\n`;

const informe = {
    completoArchivo : informeCompleto,
    completoEncabezado : encabezadoInfCompleto,
    resumidoArchivo : informeResumido,
    resumidoEncabezado : encabezadoInfResumido,
    logsArchivo : informeLogs,
    logsEncabezado : encabezadoInfLogs,
    informeDrive:informeDrive,
    sheetCompleto:sheetCompleto,
    sheetResumen:sheetResumen,
    encabezadoSheetCompleto:encabezadoSheetCompleto,
    encabezadoSheetResumen:encabezadoSheetResumen,

};

module.exports.informe = informe;
