require('dotenv').config();

const credenciales = {
    type: "service_account",
    project_id: "informesinvesting",
    private_key_id: process.env.DRIVE_PRIVATE_KEY_ID, 
    private_key: process.env.DRIVE_PRIVATE_KEY,
    client_email: "informesinvesting@informesinvesting.iam.gserviceaccount.com",
    client_id: "110109584263973298153",
    auth_uri: "https://accounts.google.com/o/oauth2/auth",
    token_uri: "https://oauth2.googleapis.com/token",
    auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
    client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/informesinvesting%40informesinvesting.iam.gserviceaccount.com"
  }
  
const folderId = process.env.DRIVE_FOLDER_ID;

module.exports={
  credenciales:credenciales,
  folderId:folderId,
}